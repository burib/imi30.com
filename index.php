<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width" />
  <title>Imi30.com</title>
  <link href='http://fonts.googleapis.com/css?family=Coming+Soon' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="static/css/foundation.min.css">
  <link rel="stylesheet" href="static/css/app.css">

  <script src="static/js/modernizr.foundation.js"></script>
</head>
<body>

  <header>
    <div class="row">
      <div class="twelve columns">
        <h1><a href="/">Imi30.com</a></h1>
        <h4><a href="/">Come in happy</a></h4>
      </div>
    </div>
  </header>
  <section id="mainContent">
    <div class="row collapse">
      <div class="twelve columns">
        <ul class="no-bullet people">
          <?php
            $people = array();

            $dir = __DIR__ . '/static/img/people/300/';
            $pattern = $dir.'{*.jpg,*.gif,*.png,*.jpeg}';

            // Open a known directory, and proceed to read its contents
            foreach(glob($pattern, GLOB_BRACE) as $file) {
                $file = pathinfo($file);
                $basename = $file['basename'];
                $people[] = "static/img/people/300/".$basename;
            }
            shuffle($people);
          ?>
        </ul>
      </div>
    </div>

  </section>

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.js"></script>
  <script src="static/js/app.js"></script>
  <script>
    var people = <?=json_encode($people) ?>;
  </script>
</body>
</html>
