;(function ($, window, undefined) {
  'use strict';

  var $doc = $(document),
      Modernizr = window.Modernizr;

    var People = function () {
      this.$peopleCont = $('.people');
      this.$people = null;
      this.peopleCount = 0;
      this.delay = 900;
      this.peopleIndex = 0;
      this.timer = null;
      this.initPeople_();
    }
    People.prototype = {
      animate_: function () {
        var _self = this;

        this.$people.eq(this.random_(this.$people.length - 8)).fadeOut(this.delay, function () {
          $(this).prop('src', _self.getPeopleSrc_()).fadeIn(_self.delay, function () {
            _self.play_();
          });
        });
      },
      play_: function () {
        var _self = this;
        setTimeout(function(){ _self.animate_()}, _self.delay);
      },
      getPeopleSrc_: function () {
        return window.people[this.random_(window.people.length)];
      },
      random_: function (maxNum) {
        return Math.floor( Math.random() * ( maxNum + 1 ) );
      },
      initPeople_: function () {
        var $doc = $(document),
            viewportHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
            peopleList = [],
            _self = this,
            i = 0;

        this.peopleCount = (Math.ceil($doc.width() / 310)+1)  * Math.ceil( (viewportHeight - $('header').height() / 2) / 310);

        for (; i < this.peopleCount; i++) {
          peopleList.push("<li><img src='"+window.people[i]+"' /></li>");
        };

        this.$peopleCont.append(peopleList);
        this.$people = this.$peopleCont.find('li').find('img').hide();
        this.showPeople_();
      },
      showPeople_: function () {
        var _self = this;

        if ( this.peopleIndex < this.peopleCount ) {
            this.$people.eq(this.peopleIndex++).fadeIn(200, function () {
              _self.timer = window.setTimeout(function () {
                _self.showPeople_.call(_self);
              }, 100);
            });
        }

        if (this.peopleIndex === this.peopleCount - 2) {
          this.animate_();
        }

      }
    };

  $(function() {
    var imisPeople = new People();
  });


  // Hide address bar on mobile devices (except if #hash present, so we don't mess up deep linking).
  if (Modernizr.touch && !window.location.hash) {
    $(window).load(function () {
      setTimeout(function () {
        window.scrollTo(0, 1);
      }, 0);
    });
  }

  // TODO: recalculate number of people to show on resize!

})(jQuery, this);
